//jshint esversion:6
const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
const app = express();
app.use(express.static("public"));
const promise = require('bluebird');
const options = {
  promiseLib: promise
};
let pgp = require('pg-promise')(options);
let port = process.env.PORT;
if (port == null || port == "") {
  port = 3000;
}

app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({
  extended: true
}));

///////////////DATABASE CONNECT//////////////

const db = pgp('postgres://qnozgqvb:bEsTHVhXZy-aubRL454jP5Wbbrg-q4Mi@balarama.db.elephantsql.com:5432/qnozgqvb');

//////////////////GET RENDER POST///////////////

app.get("/", (req, res) => {
  db.any('SELECT author, title FROM books', [true])
      .then(function(data) {
        res.render("home", {
          books: data
        });
      })
      .catch(function(error) {
          throw error;
      });
});



app.get("/book/:bookNo", (req, res) => {
  const bookNo = parseInt(req.params.bookNo);
  if (isNaN(bookNo) || bookNo < 1)
    res.render("not-found");
  else {
    db.any('SELECT b.author, b.title, a.annotation FROM books b INNER JOIN annotations a ON b.id = a.book_id WHERE b.id = ' + bookNo, [true])
        .then(function(data) {
          if (data.length===0)
            res.render("not-found");
          else
            res.render("book", {
              annots: data
            });
          })
        .catch(function(error) {
            throw error;
        });
  }
});

app.get("/random", (req, res) => {
  db.any('SELECT b.author, b.title, a.annotation FROM books b INNER JOIN annotations a ON b.id = a.book_id ORDER BY RANDOM() LIMIT 1', [true])
      .then(function(data) {
          res.render("random", {
            quote: data
          });
      })
      .catch(function(error) {
          throw error;
      });

});


app.post("/search", (req, res) => {
  const searchTerm = req.body.searchTerm;
  if (searchTerm != "")
    res.redirect("/search/" + searchTerm);
});

app.get("/search/:searchTerm", (req, res) => {
  const searchTerm = req.params.searchTerm;
  db.any("SELECT b.author, b.title, a.annotation FROM books b INNER JOIN annotations a ON b.id = a.book_id WHERE a.annotation ILIKE '%"+searchTerm+"%'", [true])
      .then(function(data) {
        if (data.length > 0) {
            res.render("search", {
              annots: data,
              searchTerm: searchTerm
            });
          } else {
            res.render("not-found");
          }
      })
      .catch(function(error) {
          throw error;
      });
});

app.get("/about", (req, res) => {
  res.render("about");
});

app.get("/:stuff", (req, res) => {
  res.render("not-found");
});


/////////////////////LISTEN///////////////////////
app.listen(port, () => {
  console.log("Server started on port " + port);
});
